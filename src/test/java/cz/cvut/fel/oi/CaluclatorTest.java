package cz.cvut.fel.oi;

import org.junit.jupiter.api.*;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CaluclatorTest {
    Calculator calculator;
    @BeforeEach
    public void setCalculator()
    {
        System.out.println("Setting up");
        calculator = new Calculator();
    }
    @Test
    @DisplayName("Test add two numbers")
    @Order(1)
    public void addTest()
    {
        int ret = calculator.add(5,6);
        Assertions.assertEquals(ret,11);
    }

    @Test
    @DisplayName("Test substract two numbers")
    @Order(2)
    public void substractTest()
    {
        int ret = calculator.subtract(6,5);
        Assertions.assertEquals(ret,1);
    }
    @Test
    @DisplayName("Test multiply two numbers")
    @Order(3)
    public void mulitplyTest()
    {
        int ret = calculator.multiply(6,3);
        Assertions.assertEquals(ret,18);
    }
    @Test
    @DisplayName("Test throws exception")
    @Order(6)
    public void throwAssertion_default_throwsAssertion(){
        Assertions.assertThrows(Exception.class, () -> calculator.throwAssertion());
    }
    @Test
    @DisplayName("Test divide two numbers")
    @Order(4)
    public void divideTest()
    {
        float ret = calculator.divide(6,3);
        Assertions.assertEquals(2,ret);
    }
    @Test
    @DisplayName("Test divide with zero")
    @Order(5)
    public void divideThrowException()
    {
        Assertions.assertThrows(Exception.class, () -> calculator.divide(6,0));
    }

}
